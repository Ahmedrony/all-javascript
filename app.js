// JavaScript Code

// Movement Animation to Happen

const card = document.querySelector('.card');
const container = document.querySelector('.container');

// Items

const title = document.querySelector('.title');
const emoji = document.querySelector('.emoji');

// Animate In

container.addEventListener('mouseenter', () => {
    card.style.transition = "none";
    // Pop Out
    title.style.transform = "translate3d(150px)";
    emoji.style.transform = "translateZ(200px) rotateZ(45deg)";
});

// Animate Out

container.addEventListener('mouseleave', () => {
    card.style.transition = "all 0.5s ease";
    card.style.transform = "rotateY(0deg) rotateX(0deg)";
    // Pop Back
    title.style.transform = "translate3d(0px)";
    emoji.style.transform = "translateZ(0px) rotateZ(0deg)";
});

function moodFunction() {
    alert('Your mood changed.');
}