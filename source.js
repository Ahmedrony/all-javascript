// JavaScript Code

const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');

form.addEventListener('submit', (e) => {
    e.preventDefault();

    checkInputs();
})

function setErrorFor(input, message) {
    const formControl = input.parentElement; //.formControl
    const small = formControl.querySelector('small');

    // Adding error message inside small tag
    small.innerText = message;

    // Adding error class
    formControl.className = 'formControl error';
}

function setSuccessFor(input) {
    const formControl = input.parentElement; //.formControl

    // Adding success class
    formControl.className = 'formControl success'
}

function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}

function checkInputs() {
    // get the values from the inputs and trimming the white spaces

    const usernameValue = username.value.trim();
    const emailValue = email.value.trim();
    const passwordValue = password.value.trim();
    const password2Value = password2.value.trim();

    if(usernameValue === '') {
        // Show Error
        setErrorFor(username, 'User Name is required');
    } else {
        // Add Success Class
        setSuccessFor(username);
    }

    if(emailValue === '') {
        setErrorFor(email, 'Email is required');
    } else if(!isEmail(emailValue)) {
        setErrorFor(email, 'Please enter a valid Email');
    } else {
        setSuccessFor(email);
    }

    if(passwordValue === '') {
        setErrorFor(password, 'Password is required');
    } else if(passwordValue.length < 8) {
        setErrorFor(password, 'Password need atleast 8 characters');
    } else {
        setSuccessFor(password);
    }

    if(password2Value === '') {
        setErrorFor(password2, 'Confirm Password is required');
    } else if(password2Value !== passwordValue) {
        setErrorFor(password2, 'Password does not match');
    } else {
        setSuccessFor(password2);
    }
}